<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Exports\CategoriesExport;


class CategoryController extends Controller
{

    public function __construct(){
        return $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cat= Category::paginate(9);
        return view('categories.index')->with('cats', $cat);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $cat = new Category;
        $cat->name  = $request->name;
        $cat->description     = $request->description;
        if ($request->hasFile('photo')) {
            $file = time().'.'.$request->image->extension();
            $request->image->move(public_path('imgs'), $file);
            $cat->image = 'imgs/'.$file;
        }

        if ($cat->save()) {
            return redirect('categories')->with('message', 'La categoría: '.$cat->name.' fue Adicionada con Exito!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
        $cat = Category::find($id);
        return view('categories.show')->with('cat', $cat);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cat = Category::find($id);
        return view('categories.edit')->with('cat', $cat);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());

        $cat = Category::find($id);
        $cat->name  = $request->name;
        $cat->description     = $request->description;
        if ($request->hasFile('image')) {
            $file = time().'.'.$request->image->extension();
            $request->image->move(public_path('imgs'), $file);
            $cat->image = 'imgs/'.$file;
        }
        if ($cat->save()) {
            return redirect('categories')->with('message', 'La Categoría: '.$cat->name.' fue Modificada con Exito!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
           //User::destroy($id)
           $cat= Category::find($id);
           if($cat->delete()){
               return redirect('categories')->with('message', 'La categoría: '.$cat->name.' fue eliminada con éxito');
           }
    }

    public function pdf(){
        $cats= Category::all();
        $pdf= \PDF::loadView('categories.pdf', compact('cats'));
        return $pdf->download('categories.pdf');
     }
 
     public function excel(){
         // $users= user::all();
         return \Excel::download(new CategoriesExport, 'categories.xlsx');
         
     }
}
