<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Reporte en PDF</title>
	<style>
		body {
			font-family: Helvetica;
		}
		table {
			border-collapse: collapse;
		}
		table th,
		table td {
			font-size: 14px !important;
		}
		table th {
			background-color: gray;
			color: white;
			text-align: center;
		}
		table td {
			border: 1px solid silver;
			padding: 10px;
		}
	</style>
</head>
<body>
	<table>
	<thead>
		<tr>
			<th> Id </th>
			<th> Nombre Completo </th>
			<th>Descripción </th>
			<th> Foto </th>
		</tr>
	</thead>
	@foreach($cats as $cat)
		<tr>
			<td> {{ $cat->id }} </td>
			<td> {{ $cat->name }} </td>
			<td> {{ $cat->description }} </td>
            <!-- <td><img src="{{public_path(). '/'. $cat->image}}" width="40px"></td> -->
			<td></td>
		</tr>
	@endforeach
</table>
</body>
</html>